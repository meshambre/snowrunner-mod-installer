﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace SnowRunnerModIo
{
    public partial class ModUserControl : UserControl
    {
        public ModUserControl()
        {
            InitializeComponent();
        }

        private Mod mod;

        public Mod Mod
        {
            get
            {
                return mod;
            }
            set
            {
                mod = value;
                textBoxId.Text = mod.id.ToString();
                textBoxName.Text = mod.name;
                radioButtonIsInstalled.Checked = mod.isInstalledLocal;
                if (mod.media.images.Count > 0)
                {
                    pictureBox1.LoadAsync(mod.media.images[0].thumb_320x180);
                }

                linkLabelURL.Text = mod.profile_url;
                linkLabelURL.Links.Add(0, linkLabelURL.Text.Length, mod.profile_url);

            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            ModIoREST.Instance.InstallMod(Mod);
        }

        private void linkLabelURL_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start(e.Link.LinkData.ToString());
        }

        private void ModUserControl_Load(object sender, EventArgs e)
        {

        }

        private void ModUserControl_BackColorChanged(object sender, EventArgs e)
        {
            textBoxId.BackColor = textBoxName.BackColor = BackColor;
        }

        private void buttonRemove_Click(object sender, EventArgs e)
        {

        }

        private void buttonDisable_Click(object sender, EventArgs e)
        {

        }
    }
}