﻿namespace SnowRunnerModIo
{
    partial class InstallSubscribedMods
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstallSubscribedMods));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.instructionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonShowSubscribed = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonInstallAll = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButtonRepairFiles = new System.Windows.Forms.ToolStripButton();
            this.panelMods = new System.Windows.Forms.Panel();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripProgressBarInstallAll = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripStatusLabelInstallAllProgress = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelInstallingModPleaseWait = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelInstallAllCompleted = new System.Windows.Forms.ToolStripStatusLabel();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            resources.ApplyResources(this.fileToolStripMenuItem, "fileToolStripMenuItem");
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            resources.ApplyResources(this.optionsToolStripMenuItem, "optionsToolStripMenuItem");
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            resources.ApplyResources(this.toolStripMenuItem1, "toolStripMenuItem1");
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            resources.ApplyResources(this.quitToolStripMenuItem, "quitToolStripMenuItem");
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.instructionsToolStripMenuItem,
            this.toolStripMenuItem2,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            resources.ApplyResources(this.helpToolStripMenuItem, "helpToolStripMenuItem");
            // 
            // instructionsToolStripMenuItem
            // 
            this.instructionsToolStripMenuItem.Name = "instructionsToolStripMenuItem";
            resources.ApplyResources(this.instructionsToolStripMenuItem, "instructionsToolStripMenuItem");
            this.instructionsToolStripMenuItem.Click += new System.EventHandler(this.instructionsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            resources.ApplyResources(this.toolStripMenuItem2, "toolStripMenuItem2");
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            resources.ApplyResources(this.aboutToolStripMenuItem, "aboutToolStripMenuItem");
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonShowSubscribed,
            this.toolStripButtonInstallAll,
            this.toolStripSeparator1,
            this.toolStripButtonRepairFiles});
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.Name = "toolStrip1";
            // 
            // toolStripButtonShowSubscribed
            // 
            resources.ApplyResources(this.toolStripButtonShowSubscribed, "toolStripButtonShowSubscribed");
            this.toolStripButtonShowSubscribed.Name = "toolStripButtonShowSubscribed";
            this.toolStripButtonShowSubscribed.Click += new System.EventHandler(this.toolStripButtonShowSubscribed_Click);
            // 
            // toolStripButtonInstallAll
            // 
            resources.ApplyResources(this.toolStripButtonInstallAll, "toolStripButtonInstallAll");
            this.toolStripButtonInstallAll.Name = "toolStripButtonInstallAll";
            this.toolStripButtonInstallAll.Click += new System.EventHandler(this.toolStripButtonInstallAll_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // toolStripButtonRepairFiles
            // 
            resources.ApplyResources(this.toolStripButtonRepairFiles, "toolStripButtonRepairFiles");
            this.toolStripButtonRepairFiles.Name = "toolStripButtonRepairFiles";
            this.toolStripButtonRepairFiles.Click += new System.EventHandler(this.toolStripButtonRepairFiles_Click);
            // 
            // panelMods
            // 
            resources.ApplyResources(this.panelMods, "panelMods");
            this.panelMods.Name = "panelMods";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripProgressBarInstallAll,
            this.toolStripStatusLabelInstallAllProgress,
            this.toolStripStatusLabelInstallingModPleaseWait,
            this.toolStripStatusLabelInstallAllCompleted});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // toolStripProgressBarInstallAll
            // 
            this.toolStripProgressBarInstallAll.Name = "toolStripProgressBarInstallAll";
            resources.ApplyResources(this.toolStripProgressBarInstallAll, "toolStripProgressBarInstallAll");
            // 
            // toolStripStatusLabelInstallAllProgress
            // 
            this.toolStripStatusLabelInstallAllProgress.Name = "toolStripStatusLabelInstallAllProgress";
            resources.ApplyResources(this.toolStripStatusLabelInstallAllProgress, "toolStripStatusLabelInstallAllProgress");
            // 
            // toolStripStatusLabelInstallingModPleaseWait
            // 
            this.toolStripStatusLabelInstallingModPleaseWait.Name = "toolStripStatusLabelInstallingModPleaseWait";
            resources.ApplyResources(this.toolStripStatusLabelInstallingModPleaseWait, "toolStripStatusLabelInstallingModPleaseWait");
            // 
            // toolStripStatusLabelInstallAllCompleted
            // 
            this.toolStripStatusLabelInstallAllCompleted.Name = "toolStripStatusLabelInstallAllCompleted";
            resources.ApplyResources(this.toolStripStatusLabelInstallAllCompleted, "toolStripStatusLabelInstallAllCompleted");
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            // 
            // InstallSubscribedMods
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panelMods);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "InstallSubscribedMods";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButtonShowSubscribed;
        private System.Windows.Forms.ToolStripButton toolStripButtonRepairFiles;
        private System.Windows.Forms.Panel panelMods;
        private System.Windows.Forms.ToolStripButton toolStripButtonInstallAll;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem instructionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBarInstallAll;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelInstallAllProgress;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelInstallingModPleaseWait;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelInstallAllCompleted;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
    }
}

