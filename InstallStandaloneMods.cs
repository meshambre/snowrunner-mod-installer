﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace SnowRunnerModIo
{
    public partial class InstallStandaloneMods : Form
    {
        public InstallStandaloneMods()
        {
            InitializeComponent();
        }

        private void InstallStandaloneMods_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void toolStripButtonAddNewMod_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.Filter = "Zip files|*.zip";
            openFileDialog1.DefaultExt = "*.zip";
            openFileDialog1.FileName = "*.zip";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                StandaloneInstall.Instance.InstallFromZip(openFileDialog1.FileName);
                UpdateModsList();
            }
        }

        private void toolStripButtonShowInstalled_Click(object sender, EventArgs e)
        {
            UpdateModsList();
        }

        static Color modBg = SystemColors.Control;


        private Color modsBackColor = modBg;
        private Color modsAltBackColor = ControlPaint.Light(modBg, 0.009f);

        string warnOriginal = null;
        private void UpdateModsList()
        {
            if (string.IsNullOrWhiteSpace(warnOriginal))
            {
                warnOriginal = toolStripLabelWarnModCount.Text;
            }

            panel1.Controls.Clear();

            Color bg = modsBackColor;
            var mods = StandaloneInstall.Instance.GetInstalledMods();
            mods.Sort(((x, y) => y.Id - x.Id));

            //for (int i = 0; i < 20; i++)
            foreach (var mod in mods)
            {
                SModUserControl sMod = new SModUserControl();

                sMod.Mod = mod;
                sMod.Dock = DockStyle.Top;
                sMod.ValueChanged += SMod_ValueChanged;
                sMod.BackColor = bg;
                if (bg == modsBackColor)
                    bg = modsAltBackColor;
                else
                    bg = modsBackColor;

                panel1.Controls.Add(sMod);
            }


            var ccc = mods.FindAll(x => x.IsEnabled).Count;
            toolStripLabelWarnModCount.Visible = ccc > 12;
            toolStripLabelWarnModCount.Text = warnOriginal.Replace("#", (ccc - 12).ToString());

        }

        private void SMod_ValueChanged(object sender, EventArgs e)
        {
            UpdateModsList();
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowOptions();
        }

        private void ShowOptions()
        {
            var cf = new ConfigForm();
            cf.ShowDialog();
        }

        private void InstallStandaloneMods_Load(object sender, EventArgs e)
        {
            UpdateModsList();
        }

        private void toolStripButtonRepairFiles_Click(object sender, EventArgs e)
        {
            StandaloneInstall.Instance.RepairFiles();
        }

        private void toolStripButtonRepairFiles_Click_1(object sender, EventArgs e)
        {
            StandaloneInstall.Instance.RepairFiles();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutForm().ShowDialog();
        }

        private void instructionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var settpath = Path.GetDirectoryName(System.Reflection.Assembly.GetAssembly(typeof(ModIoREST)).Location);
            var spath = Path.Combine(settpath, "snowrunnermodinstaller.pdf");
            try
            {
                Process.Start(spath);
            }
            catch { }

        }

        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}