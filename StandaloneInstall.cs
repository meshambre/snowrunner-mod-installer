﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace SnowRunnerModIo
{
    internal class StandaloneInstall
    {
        private static readonly Lazy<StandaloneInstall> lazy = new Lazy<StandaloneInstall>(() => new StandaloneInstall());
        public static StandaloneInstall Instance { get { return lazy.Value; } }

        private StandaloneInstall()
        {
        }

        public void RemoveMod(SMod mod)
        {
            Console.WriteLine("Remove mod");

            string path = Path.GetFullPath(Program.Settings.WorkFolder);
            string pathModIoFolder = Path.GetFullPath(path + "/../../Mods/.modio/");
            string pathModsFolder = Path.GetFullPath(path + "/../../Mods/.modio/mods/");

            var userProfile = ModIoREST.Instance.GetModsUserState();
            var xxx = userProfile.UserProfile.modStateList.Find(x => { return x.modId == mod.Id; });
            if (xxx != null)
                userProfile.UserProfile.modStateList.Remove(xxx);
            ModIoREST.Instance.WriteModStateListFile(userProfile);

            var installedModsFilename = Path.Combine(pathModIoFolder, "installed_mods.json");
            List<InstalledMod> instMods = null;

            if (!File.Exists(installedModsFilename) || File.ReadAllText(installedModsFilename).Trim() == "null")
            {
                instMods = new List<InstalledMod>();
            }
            else
            {
                instMods = JsonConvert.DeserializeObject<List<InstalledMod>>(File.ReadAllText(installedModsFilename));
            }

            var existinInstModEntry = instMods.Find(x => x.mod_id == mod.Id);
            if (existinInstModEntry != null)
            {
                instMods.Remove(existinInstModEntry);
            }
            ModIoREST.Instance.WriteInstalledModsFile(installedModsFilename, instMods);

            var modPath = Path.GetFullPath(Path.Combine(pathModsFolder, mod.Id.ToString()));
            Directory.Delete(modPath, true);
        }

        public void SetModState(SMod mod, bool state)
        {
            Console.WriteLine("Disable mod");

            var userProfile = ModIoREST.Instance.GetModsUserState();
            var xxx = userProfile.UserProfile.modStateList.Find(x => { return x.modId == mod.Id; });
            if (xxx != null)
                userProfile.UserProfile.modStateList.First(x => { return x.modId == mod.Id; }).modState = state;
            else
            {
                userProfile.UserProfile.modStateList.Add(new modStateClass { modId = mod.Id, modState = state });
            }
            ModIoREST.Instance.WriteModStateListFile(userProfile);
        }

        public List<SMod> GetInstalledMods()
        {
            List<SMod> ret = new List<SMod>();

            var modsState = ModIoREST.Instance.GetModsUserState();

            string path = Path.GetFullPath(Program.Settings.WorkFolder);
            string modsPath = Path.GetFullPath(Path.Combine(path, "../../Mods/.modio/mods"));
            Console.WriteLine(path);
            Console.WriteLine(modsPath);

            var modDirs = Directory.GetDirectories(modsPath);
            foreach (var modDir in modDirs)
            {
                Console.WriteLine(modDir);
                var modJSONPath = Path.GetFullPath(Path.Combine(modDir, "modio.json"));

                bool isPak = false;
                int modId = -1;
                if (!File.Exists(modJSONPath))
                {
                    isPak = true;
                    modId = -1;
                    int.TryParse(Path.GetFileName(modDir), out modId);
                    if (modId == -1)
                        continue;
                }
                else
                {
                    isPak = false;
                    var modInfo = JsonConvert.DeserializeObject<ModInfo>(File.ReadAllText(modJSONPath));
                    modId = modInfo.id;
                }

                SMod sMod = new SMod();
                var files = Directory.GetFiles(modDir, "*.pak");
                foreach (var file in files)
                {
                    Console.WriteLine("     {0}", Path.GetFileName(file));
                    sMod.PakFilename = Path.GetFullPath(file);
                    sMod.Name = Path.GetFileName(file);
                    sMod.Name = sMod.Name.Substring(0, sMod.Name.Length - 4);

                    if (modsState != null && modsState.UserProfile != null && modsState.UserProfile.modStateList != null)
                    {
                        var xxx = modsState.UserProfile.modStateList.Find(x => x.modId == modId);
                        if (xxx != null)
                            sMod.IsEnabled = xxx.modState;
                    }

                    sMod.IsPakInstalled = isPak;

                    break;
                }

                sMod.Id = int.Parse(Path.GetFileName(modDir));
                sMod.Folder = modDir;

                ret.Add(sMod);
            }

            return ret;
        }

        public void InstallFromZip(string zipFilename)
        {
            var zipFile = ZipFile.OpenRead(zipFilename);
            foreach (var zipEntry in zipFile.Entries)
            {
                Console.WriteLine("{0}  {1}", zipEntry.FullName, Path.GetExtension(zipEntry.FullName));

                if (Path.GetExtension(zipEntry.FullName) != ".pak")
                {
                    continue;
                }

                var pakFilename = Path.GetFileName(zipEntry.FullName);
                Console.WriteLine(pakFilename);
                var tempFile = Path.Combine(Path.GetTempPath(), pakFilename);
                Console.WriteLine(tempFile);

                zipEntry.ExtractToFile(tempFile, true);
                InstallFromPak(tempFile);
            }
        }

        private const int ID_START_NUMBER = 10000000;

        public void InstallFromPak(string pakFilename)
        {
            string path = Path.GetFullPath(Program.Settings.WorkFolder);
            string modsPath = Path.GetFullPath(Path.Combine(path, "../../Mods/.modio/mods"));
            Console.WriteLine(path);
            Console.WriteLine(modsPath);

            int maxId = -1;
            var modDirs = Directory.GetDirectories(modsPath);
            foreach (var modDir in modDirs)
            {
                Console.WriteLine(modDir);
                int modDirId = -1;
                int.TryParse(Path.GetFileName(modDir), out modDirId);
                maxId = Math.Max(maxId, modDirId);
            }
            maxId = Math.Max(maxId + 1, ID_START_NUMBER);
            Console.WriteLine("MAXID: {0}", maxId);

            var newModDir = Path.GetFullPath(Path.Combine(modsPath, maxId.ToString()));
            Console.WriteLine(newModDir);

            var newModPakFile = Path.GetFullPath(Path.Combine(newModDir, Path.GetFileName(pakFilename)));

            Console.WriteLine(newModPakFile);
            Directory.CreateDirectory(newModDir);
            File.Copy(pakFilename, newModPakFile, true);

            string pathModIoFolder = Path.GetFullPath(path + "/../../Mods/.modio/");
            string pathModsFolder = Path.GetFullPath(path + "/../../Mods/.modio/mods/");

            var installedModsFilename = Path.Combine(pathModIoFolder, "installed_mods.json");
            List<InstalledMod> instMods = null;

            if (!File.Exists(installedModsFilename) || File.ReadAllText(installedModsFilename).Trim() == "null")
            {
                instMods = new List<InstalledMod>();
            }
            else
            {
                instMods = JsonConvert.DeserializeObject<List<InstalledMod>>(File.ReadAllText(installedModsFilename));
            }

            var existinInstModEntry = instMods.Find(x => x.mod_id == maxId);
            if (existinInstModEntry == null)
            {
                instMods.Add(
                    new InstalledMod
                    {
                        path = Path.Combine(pathModsFolder, maxId.ToString()) + "/",
                        mod_id = maxId,
                        date_updated = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds
                    }
                );
            }
            ModIoREST.Instance.WriteInstalledModsFile(installedModsFilename, instMods);

            var userProfile = ModIoREST.Instance.GetModsUserState();
            if (userProfile == null)
            {
                userProfile = new UserProfileFile();
            }
            if (userProfile.UserProfile.modStateList == null)
            {
                userProfile.UserProfile.modStateList = new List<modStateClass>();
            }
            var idx = userProfile.UserProfile.modStateList.FindIndex(x => x.modId == maxId);
            if (idx >= 0)
            {
                userProfile.UserProfile.modStateList[idx].modState = true;
            }
            else
            {
                userProfile.UserProfile.modStateList.Add(new modStateClass { modId = maxId, modState = true });
            }

            ModIoREST.Instance.WriteModStateListFile(userProfile);

            //Console.WriteLine();
        }

        public void RepairFiles()
        {
            string path = Path.GetFullPath(Program.Settings.WorkFolder);
            string pathModIoFolder = Path.GetFullPath(path + "/../../Mods/.modio/");
            string pathModsFolder = Path.GetFullPath(path + "/../../Mods/.modio/mods/");

            var installedMods = GetInstalledMods();
            var installedModsFilename = Path.Combine(pathModIoFolder, "installed_mods.json");
            List<InstalledMod> instMods = new List<InstalledMod>();
            foreach (var mod in installedMods)
            {
                instMods.Add(
                    new InstalledMod
                    {
                        path = Path.Combine(pathModsFolder, mod.Id.ToString()) + "/",
                        mod_id = mod.Id,
                        date_updated = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds
                    }
                );
            }
            ModIoREST.Instance.WriteInstalledModsFile(installedModsFilename, instMods);
            var userProfile = new UserProfileFile();
            if (userProfile.UserProfile.modStateList == null)
            {
                userProfile.UserProfile.modStateList = new List<modStateClass>();
            }
            foreach (var mod in installedMods)
            {
                userProfile.UserProfile.modStateList.Add(new modStateClass { modId = mod.Id, modState = true });
            }
            ModIoREST.Instance.WriteModStateListFile(userProfile);
        }
    }
}