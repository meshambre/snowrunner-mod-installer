﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowRunnerModIo
{
    public interface IMySettings
    {
        string WorkFolder { get; set; }
        string APIKey { get; set; }
        string Email { get; set; }
        string Token { get; set; }
        DateTime TokenExpire { get; set; }
        string APIBaseURL { get; }
    }
}
