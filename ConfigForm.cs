﻿using Microsoft.WindowsAPICodePack.Dialogs;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Windows.Forms;

namespace SnowRunnerModIo
{
    public partial class ConfigForm : Form
    {
        public ConfigForm()
        {
            InitializeComponent();
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        private void ConfigForm_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            Program.Settings.WorkFolder = textBoxWorkFolder.Text;
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {
            textBoxWorkFolder.Text = Program.Settings.WorkFolder;
            textBoxApiKey.Text = Program.Settings.APIKey;
            textBoxEmail.Text = Program.Settings.Email;
            textBoxToken.Text = Program.Settings.Token;
            textBoxTokenExpire.Text = Program.Settings.TokenExpire.ToString();
            if (Program.Settings.TokenExpire < DateTime.Now)
            {
                flowLayoutPanel1.Enabled = true;
                //buttonOK.Visible = false;
            }
        }

        private void buttonGetEmailCode_Click(object sender, EventArgs e)
        {
            var client = new RestClient(Program.Settings.APIBaseURL);
            var req = new RestRequest("oauth/emailrequest", Method.POST);
            req.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            req.AddParameter("api_key", textBoxApiKey.Text);
            req.AddParameter("email", textBoxEmail.Text);
            var resp = client.Execute(req);
            Console.WriteLine(resp.Content);
            if (resp.StatusCode == HttpStatusCode.OK)
            {
                MessageBox.Show(toolStripStatusLabelSucces2.Text, toolStripStatusLabelSucces.Text);
            }
            else
            {
                MessageBox.Show(toolStripStatusLabelErrorOneTimeCode2.Text + resp.StatusCode.ToString(), toolStripStatusLabelErrorOneTimeCode.Text);
            }
        }

        private void buttonGetToken_Click(object sender, EventArgs e)
        {
            var client = new RestClient(Program.Settings.APIBaseURL);
            var req = new RestRequest("oauth/emailexchange", Method.POST);
            req.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            req.AddParameter("api_key", textBoxApiKey.Text);
            req.AddParameter("security_code", textBoxCode.Text);
            var resp = client.Execute<emailexchange>(req);

            var rr = resp.Data;
            if (rr.error != null)
            {
                //buttonOK.Visible = false;
                MessageBox.Show(toolStripStatusLabelErrorEmailCode2.Text + rr.error.code + "\n" + rr.error.message, toolStripStatusLabelErrorEmailCode.Text);
                return;
            }

            Program.Settings.TokenExpire = DateTimeOffset.FromUnixTimeSeconds(rr.date_expires).DateTime;
            textBoxToken.Text = rr.access_token;
            textBoxTokenExpire.Text = Program.Settings.TokenExpire.ToString();
            //buttonOK.Visible = true;
            Program.Settings.APIKey = textBoxApiKey.Text;
            Program.Settings.Email = textBoxEmail.Text;
            Program.Settings.Token = textBoxToken.Text;
        }

        private void textBoxEmail_TextChanged(object sender, EventArgs e)
        {
            flowLayoutPanel1.Enabled = textBoxEmail.Text != Program.Settings.Email;
        }

        private void textBoxApiKey_TextChanged(object sender, EventArgs e)
        {
            flowLayoutPanel1.Enabled = textBoxApiKey.Text != Program.Settings.APIKey;
        }

        private void buttonForceNewToken_Click(object sender, EventArgs e)
        {
            flowLayoutPanel1.Enabled = true;
        }

        private void toolStripStatusLabel1_Click(object sender, EventArgs e)
        {
            Process.Start("https://mod.io/apikey/widget");
        }

        private void buttonSelectFolder_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();

            if (Directory.Exists(textBoxWorkFolder.Text)) { dialog.InitialDirectory = textBoxWorkFolder.Text; }
            else { dialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); }

            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                textBoxWorkFolder.Text = dialog.FileName;
            }
        }
    }

    internal class games
    {
        public List<game> data { get; set; }
        internal class game
        {
            public int id { get; set; }
            public string name { get; set; }
        }
    }

    internal class emailexchange
    {
        public string access_token { get; set; }
        public long date_expires { get; set; }
        public string code { get; set; }
        public string message { get; set; }
        public errorClass error { get; set; }

        public class errorClass
        {
            public string code { get; set; }
            public string message { get; set; }
        }
    }
}